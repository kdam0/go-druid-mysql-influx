# STEP 1 build executable binary
FROM golang:1.14-alpine AS build-env

# Create new user devops
ENV USER=devops
ENV UID=10001

RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

# Get git.
RUN apk update && apk add --no-cache git

WORKDIR /go/src/app

# Add source code.
COPY src/ ./

# Get the dependencies.
RUN go get -d -v

# Build the binary.
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build \
      -ldflags='-w -s -extldflags "-static"' -a \
      -o /go/bin/app .


# STEP 2 build a small image
#FROM scratch
FROM gcr.io/distroless/static

ENV TZ=America/Toronto

COPY --from=build-env /etc/passwd /etc/passwd
COPY --from=build-env /etc/group /etc/group
COPY --from=build-env /go/bin/app /go/bin/app

# Use an unprivileged user.
USER devops:devops

#CMD ["/go/bin/app"]
ENTRYPOINT ["/go/bin/app"]
